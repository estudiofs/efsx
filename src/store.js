import Vue from 'vue'
import eFSx from './index'
console.log(eFSx)
Vue.use(eFSx)

export default new eFSx.Store(
  {
    state: {
      propertyA: 1,
      really: {
        nested: {
          property: 'hola'
        },
        notSo: 1
      },
      propertyC: 1,
      iam: {
        nested: {
          aswell: 'nested as well'
        }
      }
    },
    mutations: {
      'really.notSo': (value, state) => {
        return value * 2
      }
    },
    getters: {
      'iam.nested.aswell': (state) => {
        return '!!!' + state.iam.nested.aswell + '!!!'
      }
    }
  },
  {inmutable: true}
)