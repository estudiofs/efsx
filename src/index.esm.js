import { Store, install } from './efsX/'
import { mapToComputed } from './efsX/helpers'

export default {
  Store,
  install,
  mapToComputed,
  version: '__VERSION__'
}

export {
  Store,
  install,
  mapToComputed,
}