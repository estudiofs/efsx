export const getReference = (obj, str) => {
  if (!str) return obj
  return str.split('.').reduce((o, i) => {
    if (!o) return null
    return o[i]
  }, obj)
}

export const applyReference = (obj, str) => {
  if (!str) return obj
  return str.split('.').reduce((o, i) => {
    o[i] = {}
    return o[i]
  }, obj)
}

export const parseKey = str => {
  const arr = str.split('.')
  const last = arr.pop()
  return { path: arr.join('.'), last }
}

export const buildGetterSetter = (key, store) => {

  return {
    get() {
      store || (store = this.$store)
      return store.getter(key)
    },
    set(value) {
      store || (store = this.$store)
      store.commit(key, value)
    }
  }
}

export const suffixKey = (key, module) => {
  return (module && module + '.' + key) || key
}
