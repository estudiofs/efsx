import applyMixin from './mixin'

import {
  getReference,
  applyReference,
  parseKey,
  suffixKey,
  buildGetterSetter
} from './utils'

let Vue // bind on install

export class Store {
  state = {}
  mutations = {}
  getters = {}
  inmutable = false
  historyMode = false
  history = []

  _addState(state, module) {
    if (!state) return
    const observable = Vue.observable(state)
    if (module) {
      this.state[module] = observable
    } else {
      this.state = observable
    }
  }
  _addMutations(mutations, module) {
    if (!mutations) return

    Object.keys(mutations).forEach(key => {
      const { path, last } = parseKey(suffixKey(key, module))
      const reference = applyReference(this.mutations, path)
      reference[last] = mutations[key]
    })
  }
  _addGetters(getters, module) {
    if (!getters) return

    Object.keys(getters).forEach(key => {
      const { path, last } = parseKey(suffixKey(key, module))
      const reference = applyReference(this.getters, path)
      reference[last] = getters[key]
    })
  }

  addModule(key, module) {
    this._addState(module.state, key)
    this._addMutations(module.mutations, key)
    this._addGetters(module.getters, key)
  }

  constructor(conf, options) {
    if (conf) {
      this._addState(conf.state)
      this._addMutations(conf.mutations)
      this._addGetters(conf.getters)

      if (conf.modules) {
        Object.keys(conf.modules).forEach(key => {
          this.addModule(key, conf.modules[key])
        })
      }
    }

    this.inmutable = (options && options.inmutable) || false
    this.historyMode = (options && options.historyMode) || false

    this._saveHistory()
  }

  _saveHistory() {
    if (this.inmutable && this.historyMode) {
      this.history.push(this.state)
    }
  }

  _getMuto(key) {
    // we get a reference to the object that needs to be mutated
    let muto = getReference(this.state, key)

    // if inmutable, we replace the muto with a copy in it's parent
    if (this.inmutable) {
      muto = { ...muto }
      if (key) {
        const { path, last } = parseKey(key)
        let container = getReference(this.state, path)
        container[last] = muto
      }
    }
    return muto
  }

  commit(key, value) {
    const { path, last } = parseKey(key)
    let muto = this._getMuto(path) // gets the object that receives the modification
    let mutation = getReference(this.mutations, key) // checks if there's a mutation for this key
    const _value = mutation ? mutation(value, this.state) : value
    muto[last] = mutation ? mutation(value, this.state) : value // applies the value on the property of the object
  }

  getter(key, getRaw) {
    let getter = getReference(this.getters, key)
    let raw = getReference(this.state, key)
    if (getRaw) return raw
    return getter ? getter(this.state) : raw
  }

  mapComputed(items, module) {
    const ret = {}
    const mapped = items.forEach(e => {
      let key, mutation
      if (typeof e === 'string') {
        key = e
        mutation = e
      } else {
        key = Object.keys(e)[0]
        mutation = e[key]
      }
      ret[key] = buildGetterSetter(suffixKey(mutation, module), this)
    })
    return ret
  }
}

export function install (_Vue) {
  if (Vue && _Vue === Vue) {
    if (process.env.NODE_ENV !== 'production') {
      console.error(
        '[efsX] already installed. Vue.use(Vuex) should be called only once.'
      )
    }
    return
  }
  Vue = _Vue
  applyMixin(Vue)

}