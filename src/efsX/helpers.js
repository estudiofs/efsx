
import {
  suffixKey,
  buildGetterSetter
} from './utils'

export const  mapToComputed = (items, module) => {
  const ret = {}
  const mapped = items.forEach(e => {
    let key, mutation
    if (typeof e === 'string') {
      key = e
      mutation = e
    } else {
      key = Object.keys(e)[0]
      mutation = e[key]
    }
    ret[key] = buildGetterSetter(suffixKey(mutation, module))
  })
  return ret
}

